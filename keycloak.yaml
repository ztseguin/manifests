apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  namespace: syscom
  name: keycloak-spi-pvc
spec:
  storageClassName: cloudstack-storage
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Mi
---
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: syscom
  name: keycloak-config
data:
  DB_VENDOR: mysql
  DB_ADDR: mariadb.cloud.csclub.uwaterloo.ca
  DB_PORT: "3306"
  DB_DATABASE: keycloak
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: syscom
  name: keycloak
  labels:
    app: keycloak
spec:
  selector:
    matchLabels:
      app: keycloak
  template:
    metadata:
      labels:
        app: keycloak
    spec:
      containers:
        - name: keycloak
          image: quay.io/keycloak/keycloak:16.1.0
          volumeMounts:
            - mountPath: "/opt/jboss/keycloak/standalone/deployments"
              name: keycloak-spi-pv
              subPath: keycloak-spi
          ports:
            - name: http
              containerPort: 8080
          env:
            - name: PROXY_ADDRESS_FORWARDING
              value: "true"
            - name: DB_VENDOR
              valueFrom:
                configMapKeyRef: {"name": "keycloak-config", "key": "DB_VENDOR"}
            - name: DB_ADDR
              valueFrom:
                configMapKeyRef: {"name": "keycloak-config", "key": "DB_ADDR"}
            - name: DB_PORT
              valueFrom:
                configMapKeyRef: {"name": "keycloak-config", "key": "DB_PORT"}
            - name: DB_DATABASE
              valueFrom:
                configMapKeyRef: {"name": "keycloak-config", "key": "DB_DATABASE"}
            # e.g. kubectl -n syscom create secret generic keycloak-secret --from-literal=DB_USER=user ...
            - name: DB_USER
              valueFrom:
                secretKeyRef: {"name": "keycloak-secret", "key": "DB_USER"}
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef: {"name": "keycloak-secret", "key": "DB_PASSWORD"}
            - name: KEYCLOAK_USER
              valueFrom:
                secretKeyRef: {"name": "keycloak-secret", "key": "KEYCLOAK_USER"}
            - name: KEYCLOAK_PASSWORD
              valueFrom:
                secretKeyRef: {"name": "keycloak-secret", "key": "KEYCLOAK_PASSWORD"}
      volumes:
        - name: keycloak-spi-pv
          persistentVolumeClaim:
            claimName: keycloak-spi-pvc
---
apiVersion: v1
kind: Service
metadata:
  name: keycloak-service
  namespace: syscom
spec:
  selector:
    app: keycloak
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 8080
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: keycloak-ingress
  namespace: syscom
spec:
  rules:
  - host: keycloak.csclub.uwaterloo.ca
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: keycloak-service
            port:
              number: 8080
